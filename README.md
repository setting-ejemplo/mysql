# README #

## MYSQL ##

### Clonar repositorio ###
* git clone https://setting-ejemplo@bitbucket.org/setting-ejemplo/mysql.git

### Crear imagen
* docker build -t settingejemplo/mysql .

### Crear contenedor ###
* docker run \
  --detach \
  --name mysql-container \
  --publish 3306:3306 \
  -e MYSQL_ROOT_PASSWORD=admin \
  mysql:latest
